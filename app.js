window.onload = () => {
    const body = document.querySelector('body');
    // 1.1 Basandote en el array siguiente, crea una lista ul > li dinámicamente en el html que imprima cada uno de los paises.
    const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];
    const newUlCountries = document.createElement('ul');
    newUlCountries.id = 'countriesUl'; // 1.6
    for (let i = 0; i < countries.length; i++) {
        const newLiCo = document.createElement('li');
        newLiCo.innerText = countries[i];
        const newBtn = document.createElement('button'); // 1.6
        newBtn.innerText = 'Del Co'; // 1.6
        newBtn.id = 'btnCo'; // 1.6
        newLiCo.appendChild(newBtn); // 1.6
        newUlCountries.appendChild(newLiCo);
    }
    body.appendChild(newUlCountries);
    // 1.2 Elimina el elemento que tenga la clase .fn-remove-me.
    const fnRemove = document.querySelector('.fn-remove-me');
    fnRemove.remove();
    // 1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos en el div de html con el atributo data-function="printHere".
    const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];
    const printHere = document.querySelector('[data-function="printHere"]');
    const newUlCars = document.createElement('ul');
    newUlCars.id = 'carsUl'; // 1.6
    for (let i = 0; i < cars.length; i++) {
        const newLiCa = document.createElement('li');
        newLiCa.innerText = cars[i];
        const newBtn = document.createElement('button'); // 1.6
        newBtn.innerText = 'Del Ca'; // 1.6
        newBtn.id = 'btnCa'; // 1.6
        newLiCa.appendChild(newBtn); // 1.6
        newUlCars.appendChild(newLiCa);
    }
    printHere.appendChild(newUlCars);
    // 1.4 Crea dinamicamente en el html una lista de div que contenga un elemento h4 para el titulo y otro elemento img para la imagen.
    const countries14 = [
        { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1' },
        { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2' },
        { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3' },
        { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4' },
        { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5' },
    ];
    const newUlC14 = document.createElement('ul');
    newUlC14.id = 'divImgs';
    for (let i = 0; i < countries14.length; i++) {
        const newLiC14 = document.createElement('li');
        const div14 = document.createElement('div');
        // div14.id = 'divImgs';
        const newH4 = document.createElement('h4');
        newH4.innerText = countries14[i].title;
        const newImg = document.createElement('img');
        newImg.src = countries14[i].imgUrl;
        newLiC14.appendChild(div14);
        div14.appendChild(newH4);
        div14.appendChild(newImg);
        newUlC14.appendChild(newLiC14);
        body.appendChild(newUlC14);
    }
    // 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último elemento de la lista.
    const newBtn = document.createElement('button');
    newBtn.innerText = 'Boton';
    newBtn.id = 'btn';
    body.appendChild(newBtn);
    // 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los elementos de las listas que elimine ese mismo elemento del html.
    addEventListeners();
};

function addEventListeners() {
    // 1.5
    const btn = document.querySelector('#btn');
    const divImgs = document.querySelector('#divImgs');
    btn.addEventListener('click', () => {
        divImgs.removeChild(divImgs.lastChild);
    });
    // 1.6
    const btnCa = document.querySelectorAll('#btnCa');
    btnCa.forEach(e => {
        e.addEventListener('click', () => {
            e.parentNode.remove();
        });
    });
    const btnCo = document.querySelectorAll('#btnCo');
    btnCo.forEach(e => {
        e.addEventListener('click', () => {
            e.parentNode.remove();
        });
    });
}
